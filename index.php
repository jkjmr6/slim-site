<?php
//Make sure you include .htaccess, otherwise you won't be able to get to the pages.

require 'vendor/autoload.php';
date_default_timezone_set("America/Chicago");

// Log errors
// $log = new Logger('name');
// $log->pushHandler(new StreamHandler('app.log', Logger::WARNING));
// $log->addWarning('Foo');

//Add Twig Views
$app = new \Slim\Slim(array(
	'view' => new \Slim\Views\Twig()
));

// Get .twig files and show errors
$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
);
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
);

// Routes for our two pages (views are in templates/, main.twig is the layout file)
$app->get('/', function() use($app){
	$app->render('about.twig');
})->name('home');

$app->get('/contact', function() use($app){
	$app->render('contact.twig');
})->name('contact');

//Listen for $_POST, sanitize and redirect
$app->post('/contact', function() use($app){
	$name = $app->request->post('name');
	$email = $app->request->post('email');
	$msg = $app->request->post('msg');

	if(!empty($name) && !empty($email) && !empty($msg)) {
		$cleanName = filter_var($name, FILTER_SANITIZE_STRING);
		$cleanEmail = filter_var($email, FILTER_SANITIZE_EMAIL);
		$cleanMsg = filter_var($msg, FILTER_SANITIZE_STRING);
	}
	else{
		//Message "There was a problem"
		$app->redirect('/contact');
	}

	//Set transport object
	$transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');
	$mailer = \Swift_Mailer::newInstance($transport);

	$message = \Swift_Message::newInstance();
	//Be sure to include the subject first to prevent errors
	$message->setSubject('Email From Website');
	$message->setFrom(array(
		$cleanEmail => $cleanName
	));
	//use for local mail testing
	$message->setTo(array('jasonjones@localhost'));
	$message->setBody($cleanMsg);

	$result = $mailer->send($message);

	if($result){
		//send a thank you message
		$app->redirect('/');
	}else{
		//send a message that the message failed to send. Log the error.
		$app->redirect('/contact');
	}

});

$app->run();