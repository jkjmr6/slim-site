A simple static site built with the Slim PHP framework. It leverages Composer to included the packages: 

* Monolog
* Twig
* Twig Views
* SwiftMailer